import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-purchase-form',
  templateUrl: './purchase-form.component.html',
  styleUrls: ['./purchase-form.component.scss'],
})
export class PurchaseFormComponent implements OnInit {

  productName: string = '';
  quantity: number;
  commentary: string;

  constructor(private modalController: ModalController) { }

  ngOnInit() { }

  closeModal() {
    this.modalController.dismiss();
  }

  sendPurchase() {
    console.log(this.productName, this.quantity, this.commentary)
    this.modalController.dismiss();
  }

  updateSelect(event: any) {
    this.productName = event.detail.value;
  }

}
