import { Component } from '@angular/core';
import { GeolocationServiceService } from '../services/geolocation-service.service';
import { Router } from '@angular/router';
import { PharmaciesService } from '../services/pharmacies.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  constructor(private geolocationService: GeolocationServiceService, private router: Router, private pharmacyService: PharmaciesService) {
  }
  ionViewWillEnter() {
    this.pharmacyService.getPharmacyList()
      .subscribe(pharmacies => {
        this.pharmacies = pharmacies;
        console.log(this.pharmacies);
        this.geolocationDistance = this.geolocationService.geolocationDistance;
        this.watchPosition();
        this.nearByPharmacies = this.getNearByPharmacies(this.lastUserLatitude, this.lastUserLongitude);
      })
  }
  geolocationDistance: number;
  lastUserLatitude: number;
  lastUserLongitude: number;
  pharmacies: any[] = [];

  nearByPharmacies: any[] = [];

  watchPosition() {
    this.geolocationService.getPosition()
      .then(data => {
        console.log(data)
        this.updatePharmacies(data.coords.latitude, data.coords.longitude);
        this.lastUserLatitude = data.coords.latitude;
        this.lastUserLongitude = data.coords.longitude;
      },
        error => { console.log(error); }
      )
  };

  updatePharmacies(latitude, longitude) {
    this.geolocationDistance = this.geolocationService.geolocationDistance
    this.nearByPharmacies = this.getNearByPharmacies(latitude, longitude);
  }

  getNearByPharmacies(userLatitude: number, userLongitude) {
    let newNearByPharmacies = [];
    let distance;
    for (const pharmacy of this.pharmacies) {
      distance = this.getDistanceFromLatLonInm(userLatitude, userLongitude, pharmacy.coords.lat, pharmacy.coords.long);
      console.log(Math.floor(distance));
      if (distance < this.geolocationDistance) {
        pharmacy['distanceToWalk'] = Math.floor(distance / 100);
        newNearByPharmacies.push(pharmacy);
      }
    }
    return newNearByPharmacies;
  }
  getDistanceFromLatLonInm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in m
    return d * 1000;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }


  navigateToPharmacyDetails(index) {
    this.geolocationService.currentPharmacy = this.nearByPharmacies[index];
    this.router.navigateByUrl('/pharmacy-details');
  }
}
