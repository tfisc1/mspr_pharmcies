import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  brutPurchasePrice: number = 0;
  netPurchasePrice: number = 0;
  discountRate: number = 0;
  netSellPrice: number = 0;
  multiplyingFactor: number = 0;
  total: number = 0;
  pageOpen = 0;

  openCalculator(calculatorNumber: number) {
    this.pageOpen = calculatorNumber;
  }

  closeCalculator() {
    this.pageOpen = 0;
    this.resetData();
  }
  modelChanged(operation: number) {
    switch (operation) {
      case 1:
        this.total = Math.floor(this.brutPurchasePrice * (1 - this.discountRate));
        break;
      case 2:
        if (this.brutPurchasePrice - this.netPurchasePrice > 0) {
          this.total = Math.floor((1 - this.netPurchasePrice / this.brutPurchasePrice) * 100);
        }
        else {
          this.total = 0;
        }
        break;
      case 3:
        this.total = this.netPurchasePrice * this.multiplyingFactor;
        break;
      case 4:
        this.total = this.netSellPrice/this.netPurchasePrice;
        break;

      default:
        break;
    }
  }

  resetData() {
    this.brutPurchasePrice = 0;
    this.netPurchasePrice = 0;
    this.discountRate = 0;
    this.netSellPrice = 0;
    this.multiplyingFactor = 0
    this.total = 0;
  }
}


