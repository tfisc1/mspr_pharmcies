import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GeolocationServiceService } from '../services/geolocation-service.service';
import { ModalController } from '@ionic/angular';
import { PurchaseFormComponent } from '../purchase-form/purchase-form.component';
import { OrdersService } from '../services/orders.service';
@Component({
  selector: 'app-pharmacy-details',
  templateUrl: './pharmacy-details.page.html',
  styleUrls: ['./pharmacy-details.page.scss'],
})
export class PharmacyDetailsPage implements OnInit {

  pharmacy : any;
  orders : any[] = [];

  constructor(private geolocationService : GeolocationServiceService, private modalController : ModalController, private orderService : OrdersService) { }

  ngOnInit() {
    this.pharmacy =  this.geolocationService.currentPharmacy;
    this.orderService.getPharmacyOrdersById(this.pharmacy.id)
    .subscribe(orders => {
      this.orders = orders;
      console.log(this.orders);
    });
    console.log(this.pharmacy);
  }

  async openFormModal(){
    console.log('tu cliques');
    const modal = await this.modalController.create({
      component: PurchaseFormComponent, 
    });
    return await modal.present();
  }

}
