import { Component } from '@angular/core';
import { GeolocationServiceService } from '../services/geolocation-service.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  constructor(private geolocationService : GeolocationServiceService) { }

  geolocationDistance: number = 200;


  updateDistance(event: any) {
    this.geolocationDistance = Number(event.detail.value);
    this.geolocationService.geolocationDistance = this.geolocationDistance;
    
    
    
  }
}
