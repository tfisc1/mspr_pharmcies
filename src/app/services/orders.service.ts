import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as env from '../../environments/environment'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  private baseUrl = env.environment.url;
  private getOrders = env.environment.paths.ordersById;

  constructor(private http : HttpClient) { }

  getPharmacyOrdersById(id : number): Observable<any>{
    return this.http.get(`${this.baseUrl}${this.getOrders}${id}`);
  }
}
