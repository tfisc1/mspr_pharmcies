import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as env from '../../environments/environment'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PharmaciesService {

  private baseUrl = env.environment.url;
  private getAllPharmacies = env.environment.paths.pharmacies;
  constructor(private http : HttpClient) { }

  getPharmacyList() : Observable<any>{
    return this.http.get(`${this.baseUrl}${this.getAllPharmacies}`)
  }
}
