import { Injectable} from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeolocationServiceService {

  constructor(private geolocation: Geolocation) { }

  geolocationDistance: number = 200;
  currentPharmacy: any;
  
  getPosition(): Promise<Geoposition> {
    return this.geolocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0});
  }
  watchPosition(): Observable<Geoposition> {
    return this.geolocation.watchPosition({
      enableHighAccuracy: true,
      timeout: 20000,
      maximumAge: 0
    });
  }
}
